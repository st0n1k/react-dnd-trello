import React, {useState, useRef, useEffect} from 'react';
import Button from '../Button';
import Card from '../Card';

import add from '../../assets/add.svg';
import close from '../../assets/close.svg';

import './form.scss';

const AddForm = ({columnIndex, children, isEmptyColumn, onAddCard, onAddColumn}) => {
    const [formOpened, setFormOpened] = useState(false);
    const [value, setValue] = useState('');
    const textareaRef = useRef(null);

    useEffect(() => {
        if(textareaRef.current) {
            textareaRef.current.focus();
        }
    }, [formOpened])

    const onAdd = () => {
        if(isEmptyColumn) {
            onAddColumn(value)
        } else {
            onAddCard(columnIndex, value);
        }
        setValue('');
        setFormOpened(false);
    }

    return (
        <React.Fragment>
            {formOpened ? 
            <div className="add-form">
                <div className="add-form__input">
                    <Card><textarea onChange={e => setValue(e.target.value)} value={value} placeholder={isEmptyColumn ? "Enter card name" : "Enter column name"} ref={textareaRef} rows="3"></textarea></Card>
                    <div className="add-form__bottom">
                        <Button onClick={onAdd}>{!isEmptyColumn ? "Add new card" : "Add new column"}</Button>
                        <img onClick={() => setFormOpened(false)} className="add-form__bottom-clear" src={close} alt="Close form"/>
                    </div>
                </div>
            </div>
            :
            <div className="add-form__bottom">
                <div onClick={() => setFormOpened(true)} className="add-form__bottom-add-btn">
                    <img src={add} alt="Add button"/>
                    <span>{!isEmptyColumn ? "Add another card" : "Add another column"}</span>
                </div>
            </div>
        }
            
        </React.Fragment>
    )
}

export default AddForm;
