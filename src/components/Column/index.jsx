import React from 'react';
import './column.scss';
import Card from '../Card';
import AddForm from '../AddForm';
import remove from '../../assets/close.svg';

import { Droppable } from "react-beautiful-dnd";

const Column = ({title, cards, onAddColumn, onAddCard, columnIndex, onRemove}) => {
    const removeColumn = () => {
        if(global.confirm(`Do you want to delete column ${title}?`)) {
            onRemove(columnIndex);
        }
    }

    return cards ? (
        <Droppable type="CARDS" droppableId={`column-${columnIndex}`}>
        {provided => (
          <div
            className="column"
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            <div className="column__inner">
              {title && (
                <div className="column__title">
                  <b>{title}</b>
                  <div onClick={removeColumn} className="column__remove">
                    <img src={remove} alt="Clear svg icon" />
                  </div>
                </div>
              )}
              <div className="column__items">
                {cards.map((card, index) => (
                  <Card key={index} columnIndex={columnIndex} cardIndex={index}>
                    {card}
                  </Card>
                ))}
                {provided.placeholder}
              </div>
              <AddForm
                isEmptyColumn={false}
                columnIndex={columnIndex}
                onAddColumn={onAddColumn}
                onAddCard={onAddCard}
              />
            </div>
          </div>
        )}
      </Droppable>
    ) : (
        <div className={"column column--empty"}>
      <div className="column__inner">
        <AddForm
          isEmptyColumn={true}
          columnIndex={columnIndex}
          onAddColumn={onAddColumn}
          onAddCard={onAddCard}
        />
      </div>
    </div>
    );
}

export default Column;
