import React from 'react';
import '../style.scss';

import Columns from '../containers/Columns';

const App = () => {
    return (
        <div className="app">
            <Columns />
        </div>
    )
}

export default App;
