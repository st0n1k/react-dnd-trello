import {combineReducers} from 'redux';

import columns from './columnReducer';

export default combineReducers({
    columns
})