import reorderCards from "../components/DND/reorderCards";

const initialState = [
        {
            title: "In Process",
            cards: [
                'Make ToDo App',
                'Make Trello App',
                'Learn JavaScript React',
                'Make ToDo App',
                'Make Trello App',
                'Learn JavaScript React',
                'Make ToDo App',
                'Make Trello App'
            ]
        },
        {
            title: "Hello Test",
            cards: [
                'Make ToDo App',
                'Make Trello App',
                'Learn JavaScript React'
            ]
        }
];

export default (state = initialState, action) => {
    switch (action.type) {
        case "CARDS_ADD":
            return state.map((item, index) => {
                    if(action.payload.columnIndex === index) {
                        return {
                            ...item,
                            cards: [
                                ...item.cards,
                                action.payload.text
                            ]
                        }
                    }
                    return item;
                })
        case "COLUMN_ADD":
            return [
                ...state,
                {
                    title: action.payload,
                    cards: []
                }
            ]
        case "COLUMN_REMOVE":
            return state.filter((item, index) => action.payload !== index)
        case "CARDS:REORDER": {
            const { source, destination } = action.payload;
            return reorderCards({
                state,
                source,
                destination
            });
            }
        default:
            return state
    }
}