export default {
    addColumn: name => ({
        type: 'COLUMN_ADD',
        payload: name
    }),
    removeColumn: index => ({
        type: 'COLUMN_REMOVE',
        payload: index
    })
}