import React from 'react';
import Column from '../components/Column';
import {connect} from 'react-redux';
import columnActions from '../actions/columns'
import cardsActions from '../actions/cards'

import { DragDropContext } from "react-beautiful-dnd";

const Columns = ({ items, addColumn, addCard, removeColumn, reorderCards }) => {
    const onDragEnd = result => {
        const { source, destination } = result;
        if (
          !destination ||
          (source.droppableId === destination.droppableId &&
            source.index === destination.index)
        ) {
          return;
        }
        reorderCards({
          source,
          destination
        });
    };

    return (
        <React.Fragment>
        <DragDropContext onDragEnd={onDragEnd}>
            {items.map((item, index) => <Column {...item} key={index} columnIndex={index} onAddColumn={addColumn} onAddCard={addCard} onRemove={removeColumn} />)}
        </DragDropContext>
        <Column onAddColumn={addColumn} onAddCard={addCard} onRemove={removeColumn} />
        </React.Fragment>
    )
    
} 

export default connect(({columns}) => ({items: columns}), {...columnActions, ...cardsActions})(Columns);